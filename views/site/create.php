<?php
use  yii\helpers\Html;
use  yii\widgets\ActiveForm;
?>
<h1>Servicio de Soporte</h1>
<h3> <?=  $msg ?> </h3>
<?php $form = ActiveForm::begin([
    "method" => "post",
    'enableClientValidation' => true,
]);
?>
<div class="form-group">
    <?= $form->field($model,"user_name")->Input("text")?>
</div>
<div class="form-group">
    <?= $form->field($model,"password")->Input("password")?>
</div>
<?= Html::submitButton("Crear", ["class" => "btn btn-success"])?>

<?php $form->end() ?>
