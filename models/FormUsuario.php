<?php
namespace app\models;
use Yii;
use yii\base\model;

 class FormUsuario extends model
 {
     public $user_name;
     public $password;

     public function rules()
     {
         return
             [
                 ['user_name', 'required', 'message' => 'solo se aceptan letras'],
                 ['user_name', 'match', 'pattern' => "/^.{3,50}$/", 'message' => 'campo requerido'],
                 ['password', 'required', 'message' => 'campo requerido'],
                 ['password', 'match', 'pattern' => "/^.{3,50}$/", 'message' => 'campo requerido'],
             ];
     }
 public function attributeLabels()
 {
return
     [
    'user_name' => 'user:',
    'password' => 'password:',
 ];
 }
 }
