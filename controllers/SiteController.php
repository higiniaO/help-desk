<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FormUsuario;
use app\models\Usuario;

class SiteController extends Controller
{
    public function actionCreate()
    {
        $model = new FormUsuario;
        $msg = null;
        if ($model->load(Yii::$app->request->post())) {
          if ($model->validate())
          {
              $table = new Usuario();
              $table->user_name= $model->user_name;
              $table->password=$model->password;
              if ($table->insert())
              {
                  $msg = "registro guardado correctamente";
                  $model->user_name = null;
                  $model->password = null;
              }
              else {
                  $msg = "ha ocurrido un error al insertar el registro";
              }
          }
          else
              {
              $model->getErrors();
          }
        }

        return $this->render("create",['model'=> $model,"msg"=> $msg]);
    }
    /*public  function actionUsuario($mensaje = null)
    {
        return $this->render("usuario",["mensaje" => $mensaje]);
    }

/*public  function actionRequest()
{
    $mensaje = null;
    if (isset($_REQUEST["nombre"]))
    {
        $mensaje = " Bien , has enviado tu nombre correctamente: ". $_REQUEST["nombre"];
    }
 $this->redirect(["site/usuario" , "mensaje" => $mensaje]);
}


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
